package prova1parcial.exercicio02;

public class Usado extends Imovel {
    private float desconto;
    public float getDesconto() {
        return desconto;
    }
    public void setDesconto(float desconto) {
        this.desconto = desconto;
    }
    
    public float getPreco(){
        float preco = super.getPreco();
        float res =  preco - this.desconto;  
        return res;
    }
    
    public void imprimirDados() {
        Endereco end = this.getEndereco();
        Float preco = this.getPreco() - this.desconto;
        System.out.println("Endereço:");
        System.out.println("Logradouro:" + end.getLogradouro());
        System.out.println("número:" + end.getNumero());
        System.out.println("Complemento:" + end.getComplemento());
        System.out.println("-------------------------------");
        System.out.println("Preço:" + preco);
    }
}
