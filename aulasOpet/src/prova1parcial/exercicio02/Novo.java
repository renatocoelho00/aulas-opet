package prova1parcial.exercicio02;

public class Novo extends Imovel {

    private float valorAdicional;

    public float getValorAdicional() {
        return valorAdicional;
    }

    public void setValorAdicional(float valorAdicional) {
        this.valorAdicional = valorAdicional;
    }

    public void imprimirDados() {
        Endereco end = this.getEndereco();
        Float _preco = this.getPreco() + this.valorAdicional;
        System.out.println("Endereço:");
        System.out.println("Logradouro:" + end.getLogradouro());
        System.out.println("número:" + end.getNumero());
        System.out.println("Complemento:" + end.getComplemento());
        System.out.println("-------------------------------");
        System.out.println("Preço:" + _preco);
    }
}
