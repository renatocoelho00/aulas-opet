package prova1parcial.exercicio03;

import java.util.Scanner;

public class Principal2 {

    public static void main(String[] args) {
        Mamifero mamifero;
        Reptil reptil;
        Scanner scan = new Scanner(System.in);

        System.out.println("Mamífero");
        mamifero = (Mamifero) getDados();    
        System.out.println("Digite a quantidade de patas:");
        mamifero.setQtdPatas(Integer.parseInt(scan.nextLine()));
        
        reptil = (Reptil) getDados();
        System.out.println("É venenoso?(1 ou 0)");
        reptil.setVenenoso(Boolean.parseBoolean(scan.nextLine()));
        System.out.println("É escamoso?(1 ou 0)");
        reptil.setEscamas(Boolean.parseBoolean(scan.nextLine()));
        
        mamifero.imprimirDados();
        reptil.imprimirDados();
    }
    
    public static Animal getDados(){
        Animal animal = new Animal();
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Digite o Nome:");
        animal.setNome(scan.nextLine());
        System.out.println("Digite o comprimento:");
        animal.setComprimento(Float.parseFloat(scan.nextLine()));
        System.out.println("Digite a cor:");
        animal.setCor(scan.nextLine());
        System.out.println("Digite o ambiente:");
        animal.setAmbiente(scan.nextLine());
        System.out.println("Digite a velocidade:");
        animal.setVelocidade(Float.parseFloat(scan.nextLine()));
        
        return animal;
    }
}
