package prova1parcial.exercicio03;

public class Reptil extends Animal {

    private boolean venenoso;
    private boolean escamas;

    public boolean isVenenoso() {
        return venenoso;
    }

    public void setVenenoso(boolean venenoso) {
        this.venenoso = venenoso;
    }

    public boolean isEscamas() {
        return escamas;
    }

    public void setEscamas(boolean escamas) {
        this.escamas = escamas;
    }

    public void imprimirDados() {
        super.imprimirDados();
        String veneno = "Não";
        String escamas = "Não";
        if (this.venenoso) {
            veneno = "Sim";
        }
        if (this.escamas) {
            escamas = "Sim";
        }
        System.out.println("Venenoso:" + veneno);
        System.out.println("Escamaso:" + escamas);
    }
}
