package prova1parcial.exercicio03;

public class Animal {
    private String nome;
    private float comprimento;
    private String cor;
    private String ambiente;
    private float velocidade;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public float getComprimento() {
        return comprimento;
    }

    public void setComprimento(float comprimento) {
        this.comprimento = comprimento;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getAmbiente() {
        return ambiente;
    }

    public void setAmbiente(String ambiente) {
        this.ambiente = ambiente;
    }

    public float getVelocidade() {
        return velocidade;
    }

    public void setVelocidade(float velocidade) {
        this.velocidade = velocidade;
    }
    
    public void imprimirDados(){
        System.out.println("Nome:"+this.getNome());
        System.out.println("Comprimento:"+this.getComprimento());
        System.out.println("Cor:"+this.getCor());
        System.out.println("Ambiente:"+this.getAmbiente());
        System.out.println("Veloidade:"+this.getVelocidade());
    }
}
