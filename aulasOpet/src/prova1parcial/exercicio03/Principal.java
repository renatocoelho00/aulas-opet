package prova1parcial.exercicio03;

import java.util.Scanner;

public class Principal {

    public static void main(String[] args) {
        Mamifero mamifero = new Mamifero();
        Reptil reptil = new Reptil();
        Scanner scan = new Scanner(System.in);

        System.out.println("Mamífero");
        System.out.println("Digite o Nome:");
        mamifero.setNome(scan.nextLine());
        System.out.println("Digite o comprimento:");
        mamifero.setComprimento(Float.parseFloat(scan.nextLine()));
        System.out.println("Digite a cor:");
        mamifero.setCor(scan.nextLine());
        System.out.println("Digite o ambiente:");
        mamifero.setAmbiente(scan.nextLine());
        System.out.println("Digite a velocidade:");
        mamifero.setVelocidade(Float.parseFloat(scan.nextLine()));
        System.out.println("Digite a quantidade de patas:");
        mamifero.setQtdPatas(Integer.parseInt(scan.nextLine()));
        
        System.out.println("Réptil");
        System.out.println("Digite o Nome:");
        reptil.setNome(scan.nextLine());
        System.out.println("Digite o comprimento:");
        reptil.setComprimento(Float.parseFloat(scan.nextLine()));
        System.out.println("Digite a cor:");
        reptil.setCor(scan.nextLine());
        System.out.println("Digite o ambiente:");
        reptil.setAmbiente(scan.nextLine());
        System.out.println("Digite a velocidade:");
        reptil.setVelocidade(Float.parseFloat(scan.nextLine()));
        System.out.println("É venenoso?(1 ou 0)");
        reptil.setVenenoso(Boolean.parseBoolean(scan.nextLine()));
        System.out.println("É escamoso?(1 ou 0)");
        reptil.setEscamas(Boolean.parseBoolean(scan.nextLine()));
         
    }
}
