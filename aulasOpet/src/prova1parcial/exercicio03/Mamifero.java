package prova1parcial.exercicio03;

public class Mamifero extends Animal{
    private int qtdPatas;

    public int getQtdPatas() {
        return qtdPatas;
    }

    public void setQtdPatas(int qtdPatas) {
        this.qtdPatas = qtdPatas;
    }
    
    public void imprimirDados(){
        super.imprimirDados();
        System.out.println("Qtde. de patas:"+this.qtdPatas);
    }
}
