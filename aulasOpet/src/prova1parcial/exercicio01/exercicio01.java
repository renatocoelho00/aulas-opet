/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prova1parcial.exercicio01;

import java.util.Scanner;

/**
 *
 * @author inspetor
 */
public class exercicio01 {

   public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int horasTrab;
        float salarioHora;
        int nrDependente;
        float salarioBruto = 0;
        float salarioliquido = 0;

        float inss;
        float ir = 0;
        System.out.println("Digite a quantidade de horas trabalhadas:");
        horasTrab = Integer.parseInt(scan.nextLine());
        System.out.println("Digite o valor do salário hora:");
        salarioHora = Float.parseFloat(scan.nextLine());
        System.out.println("Digite a quantidade de dependentes:");
        nrDependente = Integer.parseInt(scan.nextLine());

        salarioBruto = horasTrab * salarioHora + (50 * nrDependente);
        if (salarioBruto <= 1000) {
            inss = salarioBruto * Float.parseFloat("8.5") / 100;
        } else {
            inss = salarioBruto * 9 / 100;
        }

        if (salarioBruto < 500) {
            ir = 0;
        }
        if (salarioBruto > 500 && salarioBruto <= 1000) {
            ir = salarioBruto*5/100;
        }
        if (salarioBruto > 1000) {
            ir = salarioBruto*7/100;
        }
        
        salarioliquido = salarioBruto-inss-ir;
        
        System.out.println("Osalário líquido é:"+salarioliquido);
    }

}
