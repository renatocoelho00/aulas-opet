package ExercicioStatic;

import java.util.Scanner;

/**
 *
 * @author Renato
 */
public class Exercicios {

    public static float calcUltimoTermoPA(float pt, float r, int nt) {
        float ut;
        
        ut = pt + (nt - 1) * r;
        return ut;
    }

    public static String verifNumPrimpo(int n) {
        int i, qtdeDiv;
        String ret;
        i = 1;
        qtdeDiv = 0;
        while (i <= n) {
            if (n % i == 0) {
                qtdeDiv++;
            }
            i++;
        }
        if (qtdeDiv == 2) {
            ret = "O nr é primo";
        } else {
            ret = "O nr não é primo";
        }
        return ret;
    }

    public static void verifImpar() {
        int n;
        Scanner scan = new Scanner(System.in);
        System.out.println("digite um numero: ");
        n = Integer.parseInt(scan.nextLine());
        if (n % 2 == 0) {
            System.out.println("O nr e par");
        } else {
            System.out.println("O nr nao e impar");
        }
    }
}
