package ExercicioStatic;

import java.util.Scanner;

/**
 *
 * @author Reanto
 */
public class principal {

    public static void main(String[] args) {
        int opc;
        Scanner scan = new Scanner(System.in);

        System.out.println("Selecione uma opção:");
        System.out.println("  1. Calcular o ultimo termo de uma P.A.");
        System.out.println("  2. Verificar se um número é primo.");
        System.out.println("  3. Verificar se um número é par.");

        opc = Integer.parseInt(scan.nextLine());
        switch (opc) {
            case 1:
                float pt, r, resultado;
                int qtdeT;

                System.out.println("Digite o primeiro termo: ");
                pt = Float.parseFloat(scan.nextLine());
                System.out.println("Digite a razao: ");
                r = Float.parseFloat(scan.nextLine());
                System.out.println("Digite a quantidade de termos da p.a.: ");
                qtdeT = Integer.parseInt(scan.nextLine());
                resultado = Exercicios.calcUltimoTermoPA(pt, r, qtdeT);
                System.out.println("O ultimo termo desta P.A. e: " + resultado);
                break;
            case 2:
                System.out.println("digite um numero: ");
                int n = Integer.parseInt(scan.nextLine());
                String res;
                res = Exercicios.verifNumPrimpo(n);
                System.out.println(res);
                break;
            case 3:
                Exercicios.verifImpar();
                break;
        }
    }

}
