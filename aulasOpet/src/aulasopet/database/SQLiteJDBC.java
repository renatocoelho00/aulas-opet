/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulasopet.database;

/**
 *
 * @author renatoc
 */
import java.sql.*;

public class SQLiteJDBC {

    private static Connection c;
    private static Statement stmt;

    public static void main(String args[]) {
        c = null;
        stmt = null;
        connect("test.db");
        inserir();
        listar();
    }

    private static void connect(String db) {
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + db);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
    }

    private static void inserir() {//int id, String name, int age, String adress, float salary) {
        try {
//            stmt = c.createStatement();
            stmt = c.createStatement();
            
            String sql ="";
            

            sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
                    + "VALUES (5, 'Teobaldino', 32, 'California', 20000.00 );";
            stmt.executeUpdate(sql);
            
            
            sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) VALUES (?, ?, ?, ?, ?);";
            String[] bindParams = {"6", "Teobaldo", "50", "Acre", "650.50"};
            stmt.executeUpdate(sql, bindParams);
            
            /*
             sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
             + "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );";
             stmt.executeUpdate(sql);

             sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
             + "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );";
             stmt.executeUpdate(sql);

             sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) "
             + "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );";
             stmt.executeUpdate(sql);
             */
            stmt.close();
            c.commit();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            //System.exit(0);
        }
        System.out.println("Records created successfully");
    }

    private static void listar() {
        try {
            c.setAutoCommit(false);

            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM COMPANY;");
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String address = rs.getString("address");
                float salary = rs.getFloat("salary");
                System.out.println("ID = " + id);
                System.out.println("NAME = " + name);
                System.out.println("AGE = " + age);
                System.out.println("ADDRESS = " + address);
                System.out.println("SALARY = " + salary);
                System.out.println();
            }
            rs.close();
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }

    private static void createTable() {
        try {
            stmt = c.createStatement();
            String sql = "CREATE TABLE COMPANY "
                    + "(ID INT PRIMARY KEY     NOT NULL,"
                    + " NAME           TEXT    NOT NULL, "
                    + " AGE            INT     NOT NULL, "
                    + " ADDRESS        CHAR(50), "
                    + " SALARY         REAL)";
            stmt.executeUpdate(sql);
            stmt.close();
            c.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Table created successfully");
    }

}
