/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aulasopet.Estados;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author renato
 */
public class principal {

    public static void main(String args[]) throws ClassNotFoundException, SQLException {
        
        Class.forName("org.sqlite.JDBC");
        Connection conn = DriverManager.getConnection("jdbc:sqlite:test.db");
        Statement stat = conn.createStatement();
        PreparedStatement prep = conn.prepareStatement("INSERT INTO ESTADO (UF, NOME) VALUES (?,?);");
        prep.setString(1, "RR");
        prep.setString(2, "Roraima");
//        prep.addBatch();
//        prep.setString(1, "MS");
//        prep.setString(2, "Mato Grosso do Sul");
//        prep.addBatch();
prep.executeUpdate();
        //conn.setAutoCommit(false);
        //prep.executeBatch();
        //conn.setAutoCommit(true);

        ResultSet rs = stat.executeQuery("select * from ESTADO;");
        while (rs.next()) {
            System.out.print("ID: " + rs.getString("ID")+"|");
            System.out.print("UF: " + rs.getString("UF")+"|");
            System.out.println("Nome: " + rs.getString("NOME"));
        }
        rs.close();
        conn.close();

    }
}
