package aulasopet.Estados.model;

import java.sql.*;

public abstract class Sqlite {
    private Connection c;
    private Statement stmt;
    
    public void connect(String db) {
        try {
            Class.forName("org.sqlite.JDBC");
            this.c = DriverManager.getConnection("jdbc:sqlite:" + db);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Banco de dados conectado com sucesso!");
    }
    
}
