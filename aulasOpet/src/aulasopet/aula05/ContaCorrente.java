package aulasopet.aula05;

import java.util.Date;
import java.util.ArrayList;

/**
 *
 * @author renatoc
 */
public class ContaCorrente {

    private String nomeCliente;
    private String conta;
    private float saldo;
    private ArrayList <String> movimentacao;

    public ContaCorrente(){
        movimentacao = new ArrayList();
    }
    
    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public boolean saque(float valor) {
        if (valor > 0 && this.saldo >= valor) {
            this.saldo = this.saldo - valor;
            this.addMovimentacao(valor,'S');
            return true;
        } else {
            return false;
        }
    }

    public void deposito(float valor) {
        if (valor > 0) {
            this.saldo = valor;
            this.addMovimentacao(valor,'D');
        }
    }
    
       
    private void addMovimentacao(float valor, char tipoMov) {
        Date data = new Date();
        String linha = tipoMov + " | ";
        linha += data + " | ";
        linha += Float.toString(valor) + " | ";
        linha += this.saldo;
        movimentacao.add(linha);
        //data.toString()
    }

    public ArrayList <String> getMovimentacao() {
        return movimentacao;
    }
    
    
}
