/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package aulasopet.aula05;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author 501429
 */
public class View {
   private static ContaCorrente contaCli;
    public static void mostramenu(ContaCorrente conta) {
        contaCli = conta;
        String txt = "";
        int opc;
        Scanner scan = new Scanner(System.in);
        txt += "Selecione uma opção:\n";
        txt += "  1. Cadastrar um Cliente.\n";
        txt += "  2. Efetuar depositoente.\n";
        txt += "  3. Efetuar saque.\n";
        txt += "  4. Mostrar movimentação.\n";
        txt += "  5. Sair.";
        msg(txt);

        opc = Integer.parseInt(scan.nextLine());

        switch (opc) {
            case 1:
                contaCli = cadastarCliente(scan);
                System.out.print(contaCli);
                mostramenu(contaCli);
                break;
            case 2:
                contaCli.deposito(efetuarDeposito(scan));
                mostramenu(contaCli);
                break;
            case 3:
                contaCli.saque(efetuarSaque(scan));
                mostramenu(contaCli);
                break;
            case 4:
                extrato(scan, contaCli);
                mostramenu(contaCli);
                break;
            case 5:
                break;
            default:
                msg("Opção inválida!");
                mostramenu(contaCli);
        }
    }

    public static ContaCorrente cadastarCliente(Scanner scan) {
        ContaCorrente contaCli = new ContaCorrente();
        msg("Digite o nome do Cliente:");
        contaCli.setNomeCliente(scan.nextLine());
        msg("Digite o número da conta corrente:");
        contaCli.setConta(scan.nextLine());
        msg("Digite o valor do depósito inicial:");
        contaCli.deposito(Float.parseFloat(scan.nextLine()));
        return contaCli;
    }

    public static float efetuarDeposito(Scanner scan) {
        msg("Digite o valor do depósito:");
        return Float.parseFloat(scan.nextLine());
    }

    public static float efetuarSaque(Scanner scan) {
        msg("Digite o valor do saque:");
        return Float.parseFloat(scan.nextLine());
    }

    public static void extrato(Scanner scan, ContaCorrente contaCli) {
        String linhas = "";
        ArrayList<String> extrato = contaCli.getMovimentacao();
        linhas += "Cliente: " + contaCli.getNomeCliente() + "\n";
        linhas += "Conta: " + contaCli.getConta() + "\n";
        linhas += "---------------------------------------------\n";
        for (String extrato1 : extrato) {
            linhas += extrato1 + "\n";
        }
        msg(linhas);
    }

    public static void msg(String txt) {
        String txtMsg = "";
        txtMsg += "#############################################\n";
        txtMsg += txt;
        txtMsg += "\n#############################################";
        System.out.println(txtMsg);
    } 
}
