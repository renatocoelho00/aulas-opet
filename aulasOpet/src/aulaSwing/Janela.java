package aulaSwing;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Janela  extends JFrame{
    JPanel painel;
    JLabel rotulo;
    
    public Janela(){
        super("Janela ola mundo!");
    }
    public void criarTela(){
        painel = new JPanel();
        rotulo = new JLabel("Ola Mundo!");
        painel.add(rotulo);
   
        getContentPane().add(painel,BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
        pack();
        setVisible(true);
    }
}
