package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author renatoc
 */
public class EstadoDao extends Estado {

    private Connection conn;
    private Statement stmt;
    private String db;

    public EstadoDao(String db) {
        this.db = db;
    }

    private void conetar() {
        try {
            Class.forName("org.sqlite.JDBC");
            this.conn = DriverManager.getConnection("jdbc:sqlite:" + this.db);
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public List<Estado> listar() {
        List<Estado> estados = new ArrayList<Estado>();
        try {
            this.conetar();
            this.conn.setAutoCommit(false);
            this.stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM ESTADO;");

            while (rs.next()) {
                Estado estado = new Estado();
                estado.setId(rs.getInt("ID"));
                estado.setUf(rs.getString("UF"));
                estado.setNome(rs.getString("NOME"));
                estados.add(estado);
            }
            rs.close();
            stmt.close();
            this.conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return estados;
    }

    public void inserir() {
        try {
            this.conetar();

            PreparedStatement stmt = this.conn.prepareStatement("INSERT INTO ESTADO (UF, NOME) VALUES (?,?);");
            stmt.setString(1, this.getUf());
            stmt.setString(2, this.getNome());

            stmt.executeUpdate();
//            stmt.close();
            this.conn.commit();
            this.conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void atualizar() {
        try {
            this.conetar();
            PreparedStatement stmt;
            stmt = this.conn.prepareStatement("UPDATE ESTADO SET UF = ?, NOME = ? WHERE ID = ?;");
            stmt.setString(1, this.getUf());
            stmt.setString(2, this.getNome());
            stmt.setString(3, String.valueOf(this.getId()));

            stmt.executeUpdate();
            stmt.close();
            this.conn.commit();
            this.conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void apagar() {
        try {
            this.conetar();
            PreparedStatement stmt = this.conn.prepareStatement("DELETE FROM  ESTADO WHERE ID = ? ;");
            stmt.setString(1, String.valueOf(this.getId()));

            this.conn.setAutoCommit(false);
            stmt.executeUpdate();
            this.conn.setAutoCommit(true);

            stmt.close();
            this.conn.commit();
            this.conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public String toString() {
        String texto = "";
        texto += "ID:" + Integer.toString(this.getId());
        texto += "\nUF:" + this.getUf();
        texto += "\nNome:" + this.getNome();
        return texto;
    }
}
