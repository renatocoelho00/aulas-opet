package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author renatoc
 */
public class UsuarioDao extends Usuario {

    private Connection conn;
    private String db;

    private void conetar() {
        try {
            Class.forName("org.sqlite.JDBC");
            this.conn = DriverManager.getConnection("jdbc:sqlite:dados.db");
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
    }

    public boolean logar() {
        try {
            this.conetar();
           //this.conn.setAutoCommit(false);

            PreparedStatement stmt = this.conn.prepareStatement("SELECT * FROM USUARIO WHERE USUARIO LIKE ? AND SENHA LIKE ?;");
            stmt.setString(1, this.getUsuario());
            stmt.setString(2, this.getSenha());
            
            ResultSet rs = stmt.executeQuery();
            rs.next();
            this.setId(rs.getInt("ID"));
            //this.setUsuario(rs.getString("USUARIO"));
            return this.getId() > 0;
        } catch (SQLException ex) {
            //Logger.getLogger(UsuarioDao.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex.getClass().getName() + ": " + ex.getMessage());
            return false;
        }
    }

    public List<Usuario> listar() {
        List<Usuario> usuarios = new ArrayList<Usuario>();
        try {
            this.conetar();
            this.conn.setAutoCommit(false);
            Statement stmt = this.conn.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM USUARIO;");

            while (rs.next()) {
                Usuario usuario = new Usuario();
                usuario.setId(rs.getInt("ID"));
                usuario.setUsuario(rs.getString("USUARIO"));
                usuarios.add(usuario);
            }
            rs.close();
            stmt.close();
            this.conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
        return usuarios;
    }

    public void inserir() {
        try {
            this.conetar();

            PreparedStatement stmt = this.conn.prepareStatement("INSERT INTO USUARIO (USUARIO, SENHA) VALUES (?,?);");
            stmt.setString(1, this.getUsuario());
            stmt.setString(2, this.getSenha());

            stmt.executeUpdate();
//            stmt.close();
            this.conn.commit();
            this.conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void atualizar() {
        try {
            this.conetar();
            PreparedStatement stmt;
            stmt = this.conn.prepareStatement("UPDATE USUARIO SET USUARIO = ?, SENHA = ? WHERE ID = ?;");
            stmt.setString(1, this.getSenha());
            stmt.setString(2, this.getUsuario());
            stmt.setString(3, String.valueOf(this.getId()));

            stmt.executeUpdate();
            stmt.close();
            this.conn.commit();
            this.conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public void apagar() {
        try {
            this.conetar();
            PreparedStatement stmt = this.conn.prepareStatement("DELETE FROM  USUARIO WHERE ID = ? ;");
            stmt.setString(1, String.valueOf(this.getId()));

            this.conn.setAutoCommit(false);
            stmt.executeUpdate();
            this.conn.setAutoCommit(true);

            stmt.close();
            this.conn.commit();
            this.conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    @Override
    public String toString() {
        String texto = "";
        texto += "ID:" + Integer.toString(this.getId());
        texto += "\nUSUARIO:" + this.getUsuario();
        return texto;
    }

}
